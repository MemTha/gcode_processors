#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#define parseDouble(s) strtod(s, NULL);

double parseDoubleIn(int* read) {
  char buf[64];
  size_t i;
  while(*read == ' ') *read = getc(stdin);
  for(i = 0;i < 64 && (*read == '-' || *read == '.' || (*read <= '9' && *read >= '0'));i++) {
    *read = getc(stdin);
  }
  buf[i] = 0;
  return parseDouble(buf);
}

int main(int argc, char** argv) {
  int read;
  uint8_t lineHasCoord = 0;
  double m[12], x, y, z;
  x = y = z = 0;
  if(argc != 13) {
    fprintf(stderr, "Usage: %s {m00} {m01} {m02} {m03} {m10} {m11} {m12} {m13} "
	    "{m20} {m21} {m22} {m23} <infile >outfile\n\tfirst row first\n", argv[0]);
    return 1;
  }
  for(size_t i = 0;i < 12;i++)
    m[i] = parseDouble(argv[i+1]);
    //printf("m[%d] = %f\n", i, m[i] = parseDouble(argv[i+1]));
  read = getc(stdin);
  while(read != EOF) {
    //printf("read=%c x=%f y=%f z=%f\n", (char)read, x, y, z);
    switch(read) {
    case 'X': case 'x': read = getc(stdin); x = parseDoubleIn(&read); lineHasCoord=1; break;
    case 'Y': case 'y': read = getc(stdin); y = parseDoubleIn(&read); lineHasCoord=1; break;
    case 'Z': case 'z': read = getc(stdin); z = parseDoubleIn(&read); lineHasCoord=1; break;
    case '\n': case '\r':
      //printf(".");
      if(lineHasCoord) {
	  printf("X%f Y%f Z%f\n",
		 x * m[0] + y * m[1] + z * m[2] + m[3],
		 x * m[4] + y * m[5] + z * m[6] + m[7],
		 x * m[8] + y * m[9] + z * m[10] + m[11]);
	lineHasCoord = 0;
      } else printf("\n");
      //read = getc(stdin);
      break;
    case EOF: return 0; break;
    default:
      printf("%c", (char)read);
      //read = getc(stdin);
      break;
    }
    read = getc(stdin);
  }
  printf("\n");
}
