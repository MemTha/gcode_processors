#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#define parseDouble(s) strtod(s, NULL);

double parseDoubleIn(int* read) {
  char buf[64];
  size_t i;
  while(*read == ' ') *read = getc(stdin);
  for(i = 0;i < 64 && (*read == '-' || *read == '.' || (*read <= '9' && *read >= '0'));i++) {
    *read = getc(stdin);
  }
  buf[i] = 0;
  return parseDouble(buf);
}

int main(int argc, char** argv) {
  int read;
  uint8_t type;
  double d[3];
  if(argc != 4) {
    fprintf(stderr, "Usage: %s {dx} {dy} {dz} <infile >outfile\n", argv[0]);
    return 1;
  }
  d[0] = parseDouble(argv[1]);
  d[1] = parseDouble(argv[2]);
  d[2] = parseDouble(argv[3]);
  //printf("dx: %f, dy: %f, dz: %f\n", d[0], d[1], d[2]);
  read = getc(stdin);
  while(read != EOF) {
    type = 0;
    while(!type) {
      switch(read) {
      case 'X': case 'x': type = 1; break;
      case 'Y': case 'y': type = 2; break;
      case 'Z': case 'z': type = 3; break;
      case EOF: type = 10; break;
      default: printf("%c", (char)read);
      }
      read = getc(stdin);
    }
    if(type == 10) break;
    while(read == ' ') read = getc(stdin);
    printf("%c%f", 'W'+type, parseDoubleIn(&read)+d[type-1]);
  }
}
