#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <regex.h>
#include <math.h>

#define MODE_RUN 4
#define MODE_APPLY 2
#define MODE_CREATE 1
#define BLOCK_SIZE 2048
#define EPSILON 0.0001
#define STEP_SIZE 0.01

#define RESPONSE_ACK 0 //ok
#define RESPONSE_DONE 1 //<Idle
#define RESPONSE_RUN 2 //<Run
#define RESPONSE_ALARM 3 //<Alarm
#define RESPONSE_DATA_PAYLOAD 4 //[...
#define RESPONSE_UNKNOWN 5 //must be last in sequence

static const char* const RESPONSES[] = {"ok", "<Idle", "<Run", "<Alarm", "[", ""};
static uint8_t verbose = 0;
static uint64_t samplesX, samplesY;
static int solutionFD;
static double sampleDx, sampleDy, minx, miny, maxx, maxy;

#define send(...) ttyCommand(ttyFD, buffer, __VA_ARGS__)

int ttyCommand(int ttyFD, char* response, uint32_t timeout, char* format, ...) {
  //first flush
  ssize_t r, offset, postdatablank, empty;
  response[0] = 0;
  if(verbose) fprintf(stderr, "\n<");
  while((r = read(ttyFD, response, BLOCK_SIZE)) > 0)
    if(verbose)
      write(2, response, r);
  va_list args;
  va_start(args, format);
  vdprintf(ttyFD, format, args);
  va_end(args);
  if(verbose) {
    fprintf(stderr, "\n>");
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
  }
  if(verbose) fprintf(stderr, "\n<");
  r = offset = postdatablank = empty = 0;
  while(postdatablank < 8 && empty < timeout) {
    usleep(1000);//1ms
    r = read(ttyFD, response + offset, BLOCK_SIZE - offset - 1);
    //if(verbose) fprintf(stderr, ".");
    if(r >= 0) {
      response[r + offset] = '\0';
      if(verbose)
	write(2, response + offset, r);
    }
    if((r > 0 && r <= strspn(response, "\n $")) || (r == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))) r = 0;
    if(r > 0) {
      offset += r;
      postdatablank = 0;
    } else if(offset) postdatablank++;
    else empty++;
  }
  r = offset;
  if(empty == timeout) {
    errno = ETIMEDOUT;
    r = -1;
    if(verbose) fprintf(stderr, "serial read timeout\n");
  }
  if(r < 0) {
    if(verbose) fprintf(stderr, "serial communication error, read got return code %ld and errno %d\n", r, errno);
    return r;
  }
  size_t i = 0;
  while(RESPONSES[i][0] && strncmp(RESPONSES[i], response, strlen(RESPONSES[i]))) i++;
  return i;
}

#define parseDouble(s) strtod(s, NULL);
#define parseInt(s) (uint32_t)strtoul(s, NULL, 0);

double surfaceDepthOfIdx(uint32_t idx) {
  lseek(solutionFD, (idx + 6) * sizeof(double), SEEK_SET);//6 doubles is size of header
  double ret = 10000;
  read(solutionFD, &ret, sizeof(double));
  if(ret < -200 || ret > 200 || ret != ret) {
    fprintf(stderr, "Warning: illogical ret value on idx %d: %f\n", idx, ret);
    ret = 0;
  }
  return ret;
}

uint32_t vertixToIdx(uint32_t x, uint32_t y) {
  if(x >= samplesX || y >= samplesY) {
    fprintf(stderr, "Illegal vertex: %d, %d\n", x, y);
    return -1;
  }
  return x * samplesY + y;
}

double surfaceDepthOfVertix(uint32_t x, uint32_t y) {
  return surfaceDepthOfIdx(vertixToIdx(x, y));
}

double surfaceDepthOfPoint(double x, double y) {
  if(x <= minx - EPSILON || x >= maxx + EPSILON || y < miny - EPSILON || y > maxy + EPSILON) {
    fprintf(stderr, "Warn: requested point outside range: %f %f\n", x, y);
  }
  x -= minx;
  y -= miny;
  double areaSum = sampleDx * sampleDy, area1, area2, area3, x1d, x2d, x3d, y3d, y2d, y1d;
  uint32_t left = (uint32_t)floor(x / sampleDx), right = left + 1,
    bottom = (uint32_t)floor(y * 2 / sampleDy), top = bottom + 1,
    x1, x2, x3, y3, y2, y1;
  //point 2 is the nearest of the 4 corners of this grid square. Point 1 is the one with the same x but other y. Point 3 has same y and other x
  if(fmod(x, sampleDx) < sampleDx * 0.5) {
    x1 = x2 = left;
    x3 = right;
  } else {
    x1 = x2 = right;
    x3 = left;
  }
  if(fmod(y, sampleDy) < sampleDy * 0.5) {
    y3 = y2 = bottom;
    y1 = top;
  } else {
    y3 = y2 = top;
    y1 = bottom;
  }
  x1d = x1 * sampleDx;
  y1d = y1 * sampleDy;
  x2d = x2 * sampleDx;
  y2d = y2 * sampleDy;
  x3d = x3 * sampleDx;
  y3d = y3 * sampleDy;
  area1 = fabs((x3d - x) * (y2d - y) - (x2d - x) * (y3d - y));
  area2 = fabs((x1d - x) * (y3d - y) - (x3d - x) * (y1d - y));
  area3 = areaSum - area2 - area1;
  return (surfaceDepthOfVertix(x1, y1) * area1 +
	  surfaceDepthOfVertix(x2, y2) * area2 +
	  surfaceDepthOfVertix(x3, y3) * area3) / areaSum;
}

typedef struct {
  double srcX, srcY, srcZ, dstX, dstY, dstZ, dX, dY, dZ, normX, normY, len;
} lineSeg;//2D

void packLineSeg(lineSeg* out, double srcX, double srcY, double srcZ, double dstX, double dstY, double dstZ) {
  out->srcX = srcX;
  out->srcY = srcY;
  out->srcZ = srcZ;
  out->dstX = dstX;
  out->dstY = dstY;
  out->dstZ = dstZ;
  out->dX = dstX - srcX;
  out->dY = dstY - srcY;
  out->dZ = dstZ - dstZ;
  out->len = sqrt(out->dX * out->dX + out->dY * out->dY);
  out->normX = out->dX / out->len;
  out->normY = out->dY / out->len;
}

int main(int argc, char** argv) {
  char* ttypath = "/dev/ttyUSB0";
  char* prefix = "";
  double safeheight = 1, probedepth = -1, probefeed = 5, val;
  char* solutionPath = "", *ingcodePath = "", *outgcodePath = "";
  uint8_t tempSolution = 0, mode = MODE_APPLY, force = 0, autobbox = 0, simulate = 0, lineHasCoord = 0;
  int ingcodeFD = 0, outgcodeFD = 1, ttyFD = 0, ret = 1;
  size_t i, j, k, lineEnd;
  ssize_t bufferEnd;
  char* buffer = 0, *commandBuffer = 0;
  for(i = 1;i < argc;i++) {
    j = 0;
    while(argv[i][j] == '-') j++;
    switch(argv[i][j]) {
    case 'v': verbose = 1; break;
    case 'h':
    default:
      printf("Usage : %s [t ttypath] [F (force, skip most checks)] [b minx maxx miny maxy] [a (auto bbox from infile)] "
	     "[s samplesPerAxis] [x samplesXAxis] [y samplesYAxis] [p probefeed] [v (verbose)] "
	     "[S samplingDistance] [X samplingDistanceX] [Y samplingDistanceY] [z safeheight] [Z probedepth] "
	     "[i ingcode] [o outgcode] [f solutionfile] [D (delete solution after applying)] "
	     "[m [C][A][R] (run create or apply. May specify any number (no delims))] [h (help)] [I (simulate)]"
	     "[T (serial timeout ms)] [k N (skip first N lines during run. Useful if a cut is aborted)]\n\t"
	     "[C 'code to run' (runs that gcode after validating the connection)]\n"
	     "\n\nWords in parenthesis are explanations of the options, not part of them.\n",
	     argv[0]);
      return 1;
    case 'C': prefix = argv[++i]; break;
    case 'T': timeout = parseInt(argv[++i]); break;
    case 'k': offset = parseInt(argv[++i]); break;
    case 'I': simulate = 1; break;
    case 'p': probefeed = parseDouble(argv[++i]); break;
    case 't': ttypath = argv[++i]+j; break;
    case 'b':
      minx = parseDouble(argv[++i]);
      maxx = parseDouble(argv[++i]);
      miny = parseDouble(argv[++i]);
      maxy = parseDouble(argv[++i]);
      autobbox = 0;
      if(!force && (minx > maxx || miny > maxy || minx < -200 || miny < -200 || maxx > 200 || maxy > 200)) {
	fprintf(stderr, "BBox seems insane. Cancelling. If it is really correct, add the F flag before the b arg\n");
	return 1;
      }
      break;
    case 'F': force = 1; break;
    case 'a': autobbox = 1; break;
    case 's': samplesX = samplesY = parseDouble(argv[++i]); sampleDx = sampleDy = 0; break;
    case 'x': samplesX = parseDouble(argv[++i]); sampleDx = 0; break;
    case 'y': samplesY = parseDouble(argv[++i]); sampleDy = 0; break;
    case 'S': sampleDy = sampleDx = parseDouble(argv[++i]); samplesX = samplesY = 0; break;
    case 'X': sampleDx = parseDouble(argv[++i]); samplesX = 0; break;
    case 'Y': sampleDy = parseDouble(argv[++i]); samplesY = 0; break;
    case 'z':
      safeheight = parseDouble(argv[++i]);
      if(!force && (safeheight < -20 || safeheight > 20)) {
	fprintf(stderr, "safeheight seems insane. Cancelling. If it is really correct, add the F flag before the z arg\n");
	return 1;
      }
      break;
    case 'Z':
      probedepth = parseDouble(argv[++i]);
      if(!force && (safeheight < -20 || safeheight > 20)) {
	fprintf(stderr, "probedepth seems insane. Cancelling. If it is really correct, add the F flag before the Z arg\n");
	return 1;
      }
      break;
    case 'i': ingcodePath = argv[++i]; break;
    case 'o': outgcodePath = argv[++i]; break;
    case 'f': solutionPath = argv[++i]; break;
    case 'D': tempSolution = 1; break;
    case 'm': mode = 0;
      i++;
      for(j = 0;argv[i][j];j++)
	switch(argv[i][j]) {
	case 'A': mode |= MODE_APPLY; break;
	case 'C': mode |= MODE_CREATE; break;
	case 'R': mode |= MODE_RUN; break;
	}
      break;
    }
  }
  if(!solutionPath[0]) {
    char template[30];
    strcpy((char*)&template, "distortionsolution.XXXXXX");
    solutionFD = mkstemp((char*)&template);
    solutionPath = template;
    if(tempSolution) unlink(solutionPath);
  } else {
    solutionFD = open(solutionPath, (mode & MODE_CREATE) ? O_RDWR | O_CREAT | O_TRUNC : O_RDONLY, 0664);
  }
  if(!solutionFD) {
    fprintf(stderr, "Solution file open failed\n");
    goto cleanup;
  }
  if(verbose) fprintf(stderr, "file paths:\n\ttty: %s\n\tsolution: %s\n\tin gcode: %s\n\tout gcode: %s\n"
		      "sampling:\n\txSamples: %ld\n\tySamples: %ld\n\tDx: %f\n\tDy: %f\n\tFeed: %f\n"
		      "bbox:\n\tauto: %d\n\tminx: %f\n\tminy: %f\n\tmaxx: %f\n\tmaxy: %f\n\tZup: %f\n\tZdown: %f\n"
		      "operation:\n\tdelete solution: %d\n\tmode: %d\n\tforced: %d\n\tsimulate: %d\n\t"
		      "skip lines: %d\n\ttimeout: %d\n\t\n",
		      ttypath, solutionPath, ingcodePath, outgcodePath,
		      samplesX, samplesY, sampleDx, sampleDy, probefeed,
		      autobbox, minx, miny, maxx, maxy, safeheight, probedepth,
		      tempSolution, mode, force, simulate,
		      offset, timeout);
  buffer = (char*)malloc(BLOCK_SIZE);
  commandBuffer = (char*)malloc(BLOCK_SIZE);
  if(autobbox) {
    minx = miny = 20000;
    maxx = maxy = -20000;
    ingcodeFD = open(ingcodePath, O_RDONLY);
    if(ingcodeFD < 0) {
      fprintf(stderr, "Failed to open input file %s", ingcodePath);
      goto cleanup;
    }
    if(ingcodeFD == 0 && mode != MODE_CREATE) {
      fprintf(stderr, "Error, cannot pre-parse ingcode for bbox because ingcode is stdin\n");
      goto cleanup;
    }
    i = bufferEnd = lineEnd = 0;
    while(1) {
      while(i >= bufferEnd) {
	if(i >= BLOCK_SIZE) {
	  fprintf(stderr, "Could not buffer line. Canceling.\n");
	  goto cleanup;
	}
	j = read(ingcodeFD, buffer + i, BLOCK_SIZE-i);
	if(j <= 0) goto filescandone;
	bufferEnd += j;
      }
      if(buffer[i] == '\n') {
	buffer[i] = '\0';
	for(j = 0;j < i;j++) {
	  if(!(buffer[j+1] <= '9' && buffer[j+1] >= '0') && buffer[j+1] != '-') continue;
	  switch(buffer[j]) {
	  case 'X': case 'x':
	    val = parseDouble(buffer+j+1);
	    if(val < minx) {
	      minx = val;
	      if(verbose) fprintf(stderr, "New minx: %f from line %s\n", val, buffer);
	    }
	    if(val > maxx) {
	      maxx = val;
	      if(verbose) fprintf(stderr, "New maxx: %f from line %s\n", val, buffer);
	    }
	    break;
	  case 'Y': case 'y':
	    val = parseDouble(buffer+j+1);
	    if(val < miny) {
	      miny = val;
	      if(verbose) fprintf(stderr, "New miny: %f from line %s\n", val, buffer);
	    }
	    if(val > maxy) {
	      maxy = val;
	      if(verbose) fprintf(stderr, "New maxy: %f from line %s\n", val, buffer);
	    }
	    break;
	  }
	}
	i++;
	memmove(buffer, buffer + i, bufferEnd - i);
	bufferEnd -= i;
	i = 0;
      } else i++;
    }
  filescandone:
    fprintf(stderr, "Detected bbox: %f %f %f %f (will add margins to + edges)\n", minx, maxx, miny, maxy);
    maxx += sampleDx;
    maxy += sampleDy;
  }
  if(mode & MODE_CREATE) {
    if(sampleDy <= EPSILON) sampleDy = (maxy - miny) / ((double)samplesY - 1);
    else samplesY = (uint64_t)floor((maxy - miny) / sampleDy + 1 - EPSILON);
    if(sampleDx <= EPSILON) sampleDx = (maxx - minx) / ((double)samplesX - 1);
    else samplesX = (uint64_t)floor((maxx - minx) / sampleDx + 1 - EPSILON);
    maxx += EPSILON;
    maxy += EPSILON;
    if(!simulate) {
      ttyFD = open(ttypath, O_RDWR | O_NONBLOCK);
      if(ttyFD < 0) {
	fprintf(stderr, "Could not open tty: %d\n", errno);
	goto cleanup;
      }
      struct termios settings;
      tcgetattr(ttyFD, &settings);
      cfsetspeed(&settings, 115200);
      //other settings here if needed
      tcsetattr(ttyFD, TCSANOW, &settings);
      tcflush(ttyFD, TCOFLUSH);
      usleep(100);
      if(verbose) fprintf(stderr, "handshake: ");
      if(send(100, "?\n") != RESPONSE_DONE) {
	fprintf(stderr, "\nHandshake failed.\n");
	goto cleanup;
      }
      send(timeout, "%s\n", prefix);
    } else {
      ttyFD = 2;
      fprintf(stderr, "%s\n", prefix);
    }
    regex_t probe;
    regmatch_t pmatches[3];
    if(regcomp(&probe, "\[PRB:[0-9.-]*,[0-9.-]*,([0-9.-]*):1]", REG_EXTENDED)) {
      fprintf(stderr, "Failed to compile regex\n");
      goto cleanup;
    }
    write(solutionFD, &minx, sizeof(double));
    write(solutionFD, &maxx, sizeof(double));
    write(solutionFD, &miny, sizeof(double));
    write(solutionFD, &maxy, sizeof(double));
    write(solutionFD, &sampleDx, sizeof(double));
    write(solutionFD, &sampleDy, sizeof(double));
    i = 0;
    for(int sx = 0;sx < samplesX;sx++) {
      double x = minx + sx * sampleDx;
      for(int sy = 0;sy < samplesY;sy++) {
	double y = miny + sy * sampleDy;
	if(!simulate) {
	  if((send(1000, "G0 Z%f\n", safeheight) != RESPONSE_ACK) ||
	     (send(1000, "G0 X%f Y%f\n", x, y) != RESPONSE_ACK) ||
	     (send(100000, "G38.2 Z%f F%f\n", probedepth, probefeed) != RESPONSE_DATA_PAYLOAD) ||
	     regexec(&probe, buffer, 3, pmatches, 0)) {
	    fprintf(stderr, "Probe apparently failed; response: %s\n", buffer);
	    goto cleanup;
	  }
	  buffer[pmatches[1].rm_eo] = '\0';
	  val = parseDouble(buffer + pmatches[1].rm_so);
	  write(solutionFD, &val, sizeof(double));
	  if(verbose) fprintf(stderr, "Probe result at X:%f Y:%f = %f\n", x, y, val);
	} else {
	  dprintf(2, "G0 Z%f\nG0 X%f Y%f\nG38.2 Z%f F%f\n", safeheight, x, y, probedepth, probefeed);
	}
      }
      i++;
    }
  }
  if(mode & MODE_APPLY) {
    outgcodeFD = open(outgcodePath, O_WRONLY | O_CREAT | O_TRUNC, 0664);
    if(outgcodeFD < 0) {
      fprintf(stderr, "Failed to create output gcode file %s with return code %d and errno %d\n",
	      outgcodePath, outgcodeFD, errno);
      goto cleanup;
    }
    if(!solutionFD && solutionPath[0]) {
      solutionFD = open(solutionPath, O_RDONLY);
    }
    if(!solutionFD) {
      fprintf(stderr, "Solution file open failed\n");
      goto cleanup;
    }
    lseek(solutionFD, 0, SEEK_SET);
    i = sizeof(double);
    if(read(solutionFD, buffer, 6*i) != 6*i) {
      fprintf(stderr, "Failed to read from solution file\n");
      goto cleanup;
    }
    memcpy(&minx, buffer, i);
    memcpy(&maxx, buffer + i * 1, i);
    memcpy(&miny, buffer + i * 2, i);
    memcpy(&maxy, buffer + i * 3, i);
    memcpy(&sampleDx, buffer + i * 4, i);
    memcpy(&sampleDy, buffer + i * 5, i);
    samplesX = (uint64_t)floor((maxx - minx) / sampleDx + 1 - EPSILON);
    samplesY = (uint64_t)floor((maxy - miny) / sampleDy + 1 - EPSILON);
    uint32_t samples = samplesX * samplesY;
    if(!force) {
      struct stat solutionStats;
      if(fstat(solutionFD, &solutionStats)) {
	fprintf(stderr, "stat solution file failed\n");
	goto cleanup;
      }
      if(solutionStats.st_size != (samples+6)*i) {
	fprintf(stderr, "solution file size seems wrong. Is: %ld; Should be: %ld\n", solutionStats.st_size, (samples+6)*i);
	goto cleanup;
      }
    }
    if(ingcodeFD > 0) {
      lseek(ingcodeFD, 0, SEEK_SET);
    } else if(ingcodeFD < 0 || ingcodePath[0]) {
      ingcodeFD = open(ingcodePath, O_RDONLY);
    }
    i = 0;
    bufferEnd = 0;
    double x = 0, y = 0, z = 10, lastX = 0, lastY = 0, lastZ = 10;
    lineSeg line;
    uint64_t subsegIdx, commandIdx = 0;
    while(1) {
      while(i >= bufferEnd) {
	if(i >= BLOCK_SIZE) {
	  fprintf(stderr, "Could not buffer line. Canceling.\n");
	  goto cleanup;
	}
	j = read(ingcodeFD, buffer + i, BLOCK_SIZE-i);
	if(j <= 0) goto filemutatedone;
	bufferEnd += j;
      }
      if(buffer[i] == '\n') {
	commandIdx++;
	buffer[i] = '\0';
	lineHasCoord = 0;
	k = 0;
	for(j = 0;j < i;j++) {
	  switch(buffer[j]) {
	  case 'X': case 'x': x = parseDouble(buffer+j+1); break;
	  case 'Y': case 'y': y = parseDouble(buffer+j+1); break;
	  case 'Z': case 'z': z = parseDouble(buffer+j+1); break;
	  case '(': goto skipLine;//gcode comment line
	  default: commandBuffer[k++] = buffer[j]; continue;
	  }
	  lineHasCoord = 1;
	  while((buffer[j+1] <= '9' && buffer[j+1] >= '0') || buffer[j+1] == '-' || buffer[j+1] == '.') j++;
	}
	commandBuffer[k] = '\0';
	if((z <= 1 || lastZ <= 1) && lineHasCoord) {
	  dprintf(outgcodeFD, "%s\n", commandBuffer);
	  packLineSeg(&line, lastX, lastY, lastZ, x, y, z);
	  if(!force && (x < minx || y < miny || x > (samplesX-1) * sampleDx + maxx || y > (samplesY-1) * sampleDy + maxy)) {
	    fprintf(stderr, "Warnging: point is outside sampling boundary. Use force flag to attempt interpolation. (%f, %f)\n",
		    x, y);
	    goto cleanup;
	  }
	  if(verbose)
	    fprintf(stderr, "%ld: Processing new input segment: (%f, %f, %f) -> (%f, %f, %f)\n",
		    commandIdx, lastX, lastY, lastZ, x, y, z);
	  for(subsegIdx = 0;subsegIdx * STEP_SIZE < line.len;subsegIdx++) {
	    x = line.srcX + line.normX * STEP_SIZE * subsegIdx;
	    y = line.srcY + line.normY * STEP_SIZE * subsegIdx;
	    z = (subsegIdx * STEP_SIZE / line.len) * line.dZ + line.srcZ + surfaceDepthOfPoint(x, y);
	    dprintf(outgcodeFD, "X%f Y%f Z%f\n", x, y, z);
	    if(verbose) fprintf(stderr, "%ld.%ld: Intermediate segment: (%f, %f, %f)\n", commandIdx, subsegIdx, x, y, z);
	  }
	  lastX = x = line.dstX;
	  lastY = y = line.dstY;
	  z = line.dstZ + surfaceDepthOfPoint(x, y);
	  dprintf(outgcodeFD, "X%f Y%f Z%f\n", x, y, z);
	  if(verbose) fprintf(stderr, "%ld.%ld: Final segment: (%f, %f, %f)\n",
			      commandIdx, subsegIdx, x, y, z);
	  lastZ = z = line.dstZ;
	} else {
	  dprintf(outgcodeFD, "%s\n", buffer);
	  lastX = x;
	  lastY = y;
	  lastZ = z;
	}
      skipLine:
	i++;
	memmove(buffer, buffer + i, bufferEnd - i);
	bufferEnd -= i;
	i = 0;
      } else i++;
    }
  filemutatedone:
    fprintf(stderr, "file write done\n");
  }
  if(mode & MODE_RUN) {
    if(!simulate && !ttyFD) {
      ttyFD = open(ttypath, O_RDWR | O_NONBLOCK);
      if(ttyFD < 0) {
	fprintf(stderr, "Could not open tty: %d\n", errno);
	goto cleanup;
      }
      struct termios settings;
      tcgetattr(ttyFD, &settings);
      cfsetspeed(&settings, 115200);
      //other settings here if needed
      tcsetattr(ttyFD, TCSANOW, &settings);
      tcflush(ttyFD, TCOFLUSH);
      usleep(100);
      if(verbose) fprintf(stderr, "handshake: ");
      if(send(100, "?\n") != RESPONSE_DONE || send(100, "M3 S100\nM5\n") != RESPONSE_ACK) {
	send(100, "M5\nM2\n");
	fprintf(stderr, "\nHandshake failed (in run block).\n");
	goto cleanup;
      }
      send(timeout, "%s\n", prefix);
    } else if(!simulate) {
      send(100, "M3 S100\nM5\n");
    } else {
      fprintf(stderr, "%s\n", prefix);
    }
    if(outgcodeFD) close(outgcodeFD);
    outgcodeFD = open(outgcodePath, O_RDONLY);
    k = i = bufferEnd = 0;
    while(1) {
      while(i >= bufferEnd) {
	if(i >= BLOCK_SIZE) {
	  fprintf(stderr, "Could not buffer line. Canceling.\n");
	  goto cleanup;
	}
	j = read(outgcodeFD, commandBuffer + i, BLOCK_SIZE-i);
	if(j <= 0) goto filedumpdone;
	bufferEnd += j;
      }
      if(commandBuffer[i] == '\n') {
	k++;
	commandBuffer[i] = '\0';
	if(commandBuffer[0] != '(' && strncmp(commandBuffer, "M2", 2) && strncmp(commandBuffer, "M02", 3) && k > offset) {
	  if(simulate) {
	    dprintf(2, "%s\n", commandBuffer);
	  } else {
	    if(send(timeout, "%s\n", commandBuffer) != RESPONSE_ACK) {
	      //PANIC
	      fprintf(stderr, "Command execution failed during cutting. PANIC\nCommand: %s (line: %ld, offset: %ld)\n"
		      "\tResponse: %s\nSending abort commands...\n",
		      commandBuffer, k, lseek(outgcodeFD, 0, SEEK_CUR), buffer);
	      send(10, "G0 Z%f\n", safeheight);
	      send(10, "M5\nM0\nM2\n");
	      goto cleanup;
	    }
	  }
	}
	i++;
	memmove(commandBuffer, commandBuffer + i, bufferEnd - i);
	bufferEnd -= i;
	i = 0;
      } else i++;
    }
  filedumpdone:
    fprintf(stderr, "Done\n");
  }
  ret = 0;
 cleanup:
  if(ttyFD && ttyFD != 1) close(ttyFD);
  if(ingcodeFD) close(ingcodeFD);
  if(outgcodeFD && outgcodeFD != 1) close(outgcodeFD);
  if(solutionFD) close(solutionFD);
  if(buffer) free(buffer);
  if(commandBuffer) free(commandBuffer);
  return ret;
}

