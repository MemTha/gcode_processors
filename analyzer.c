#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <string.h>

#define CMD_F 1
#define CMD_G0 2
#define CMD_G1 3
#define CMD_IGNORE 0
#define CMD_G_INCOMP 4
#define CMD_P 5
#define CMD_S 6
#define CMD_M 7
#define P_IGNORE 0
#define P_WAIT_MS 1
#define P_WAIT_S 2
#define POSITION_REL 1
#define POSITION_ABS 2

#define AXIS_EDGE_DEPTH 10
#define DEBUG_AXIS 5

#define parseDouble(s) strtod(s, NULL);

uint64_t parseInt(char* str) {
  uint64_t ret = 0;
  for(size_t i = 0;str[i];i++)
    ret = ret * 10 + str[i] - '0';
  return ret;
}

typedef struct {
  double val;
  uint64_t qty;
} mod_entry_t;

typedef struct {
  mod_entry_t min[AXIS_EDGE_DEPTH], max[AXIS_EDGE_DEPTH];
  double lastVal, currVal;
} axis_t;

static void initAxis(axis_t* axis) {
  size_t i;
  axis->lastVal = axis->currVal = 0;
  for(i = 0;i < AXIS_EDGE_DEPTH;i++) {
    axis->min[i].val = INFINITY;
    axis->max[i].val = -INFINITY;
    axis->min[i].qty = axis->max[i].qty = 0;
  }
  axis->currVal = axis->lastVal = 0;
}

static axis_t axes[3];
static double g1Speed, g0Speed, time, travel, vectorWidth, cutTime, cutTravel;
static size_t moveType, pType, sType, posType = POSITION_ABS, cutting = 0;
static FILE *vectorOut = 0;
static uint64_t dumpI = 0;
//static float* rastarBuffer = 0;

static void pushValToBbox(double val, axis_t* axis) {
  size_t i;
  for(i = 0;i < AXIS_EDGE_DEPTH && val > axis->min[i].val;i++);
  if(i < AXIS_EDGE_DEPTH) {
    if(val == axis->min[i].val) axis->min[i].qty++;
    else {
      if(i+1 < AXIS_EDGE_DEPTH) memmove(axis->min+i+1, axis->min+i, sizeof(mod_entry_t) * (AXIS_EDGE_DEPTH-i-1));
      axis->min[i].qty = 1;
      axis->min[i].val = val;
    }
  }
  for(i = 0;i < AXIS_EDGE_DEPTH && val < axis->max[i].val;i++);
  if(i < AXIS_EDGE_DEPTH) {
    if(val == axis->max[i].val) axis->max[i].qty++;
    else {
      if(i+1 < AXIS_EDGE_DEPTH) memmove(axis->max+i+1, axis->max+i, sizeof(mod_entry_t) * (AXIS_EDGE_DEPTH-i-1));
      axis->max[i].qty = 1;
      axis->max[i].val = val;
    }
  }
  switch(posType) {
  case POSITION_ABS: axis->currVal = val; break;
  case POSITION_REL: axis->currVal += val; break;
  }
}

static size_t processVal(double val, size_t axis, size_t command) {
  //TODO ??push if axis is already moved since last push?? (i.e. "X200Y50X250" ... test what that would do)
  if(axis <= 2) pushValToBbox(val, &axes[axis]);
  switch(command) {
  case CMD_M:
    switch((int)round(val)) {
    default: fprintf(stderr, "WARN: unknown M command: %d\n", (int)round(val)); break;
    case 6:
      fprintf(stderr, "WARN: ignoring tool change (M6)\n");
    case 30: case 2: return 1;
    case 5: cutting = 0; break;
    case 4://resume cutting
    case 3: cutting = 1; break;
    }
  case CMD_F: g1Speed = val; break;
  case CMD_G_INCOMP:
    switch((int)round(val*10)) {//because one decimal (e.g. G38.5) is valid G cmd, but never more then one place right of decimal.
    case 382: //depth probe with speed control, actual travel will be less
    case 10: moveType = CMD_G1; break;
    case 0: moveType = CMD_G0; break;
    case 40: pType = P_WAIT_MS; sType = P_WAIT_S; break;
    case 900: posType = POSITION_ABS; break;
    case 910: posType = POSITION_REL; break;
    case 210: //use mm
    case 200: //use inches
      break;
    default: fprintf(stderr, "WARN: unknown G command type: '%lf' (%lf->%d); ignoring\n", val, round(val*10), (int)round(val*10));
    }
    break;
  case CMD_P:
  case CMD_S:
    switch((command == CMD_P) ? pType : sType) {
    case P_WAIT_S: time += val/60; break;
    case P_WAIT_MS: time += val/60000; break;
    }
    break;
  }
  return 0;
}

static void processPush() {//when a whitespace character is found, signifying command cluster delimitation
  size_t i;
  double dstSqr = 0, val, dst, speed, A[3], B[3], C[3];
  for(i = 0;i < 3;i++) {
    val = axes[i].lastVal - axes[i].currVal;
    A[i] = axes[i].lastVal;
    B[i] = axes[i].currVal;
    dstSqr += val*val;
  }
  if(dstSqr <= 0.0001) return;
  dst = sqrt(dstSqr);
  //printf("Sub distance: %lf\n", dst);
  switch(moveType) {
  case CMD_G0: speed = g0Speed; break;
  default:
  case CMD_G1: speed = g1Speed; break;
  }
  travel += dst;
  time += dst/speed;
  if(cutting) {
    cutTravel += dst;
    cutTime += dst/speed;
  }
  //if(rastarBuffer) {}
  if(vectorOut) {
    C[0] = vectorWidth * (A[1]-B[1]) / dst;
    C[1] = vectorWidth * (B[0]-A[0]) / dst;
    C[2] = 0;//vectorWidth * () / dst;
    fprintf(vectorOut, "v %lf %lf %lf\nv %lf %lf %lf\nv %lf %lf %lf\nv %lf %lf %lf\nf %ld//1 %ld//1 %ld//1 %ld//1\n"
	    "# A: %lf %lf %lf  B: %lf %lf %lf  C: %lf %lf %lf  vW: %lf\n",
	    A[0] - C[0], A[1] - C[1], A[2] - C[2], B[0] + C[0], B[1] + C[1], B[2] + C[2],
	    B[0] - C[0], B[1] - C[1], B[2] - C[2], A[0] + C[0], A[1] + C[1], A[2] + C[2],
	    dumpI * 4 + 1, dumpI * 4 + 4, dumpI * 4 + 2, dumpI * 4 + 3,
	    A[0], A[1], A[2], B[0], B[1], B[2], C[0], C[1], C[2], vectorWidth);
    dumpI++;
  }
  //TODO update visualizations
  for(i = 0;i < 3;i++) {
    axes[i].lastVal = axes[i].currVal;
  }
}

int main(int argc, char** argv) {
  size_t i, j;
  double val, mul, digit;
  size_t axis = 4;//4=none/unknown
  size_t commandType = CMD_IGNORE;
  int read;
  char* tempStr;
  uint8_t toolWarned = 0;
  //uint8_t rastarMode;
  //uint64_t w = 1;
  time = travel = cutTime = cutTravel = 0;
  tempStr = (char*)malloc(4096);
  if(!tempStr) return -ENOMEM;
  g1Speed = g0Speed = 10000;
  for(i = 1;i < argc;i++) {
    for(j = 0;argv[i][j] == '-';j++);
    if(!argv[i][j]) continue;//ignore '^-*$'
    switch(argv[i][j]) {
    case 'h':
      printf("Usage: %s [h[...]] [3{filename} [p{units}]]\n\th\thelp"// [{t|b}[mapfile] w{pixels}]
	     "\n\t3{file}\tmake 3d file (.obj) pathway to file with given fs path"
	     "\n\tp{units}\tmake toolpath {units} units wide for 3\n\tt[textmap]\tdump rough visual to textmap as asciiart"
	     //"\n\tb\tmake bitmap in PGM format to file"
	     //"\n\tw{pixels/chars}\twidth of a gcode unit for mapfile, in chars for t, in pixels for b\n"
	     , argv[0]);
      free(tempStr);
      return 0;
    case '3':
      strcpy(tempStr, &argv[i][j+1]);
      if(vectorOut) {
	fprintf(stderr, "WARN: not outputting to previously supplied vector file. Only one vector file may be supplied.\n");
	fclose(vectorOut);
      }
      vectorOut = fopen(tempStr, "w");
      if(!vectorOut) {
	fprintf(stderr, "Error: opening outout file for writing: %d\n", errno);
	return -errno;
      }
      break;
    case 'b':
    case 't':
      fprintf(stderr, "Rastar output not yet implemented\n");
      return 1;
      /*
      rastarMode = argv[i][j];
      strcpy(tempStr, &argv[i][j+1]);
      if(rastarOut) {
	fprintf(stderr, "WARN: not outputting to previously supplied rastar file. Only one rastar file may be supplied.\n");
	fclose(rastarOut);
      }
      rastarOut = fopen(tempStr, "w");
      if(!rastarOut) {
	fprintf(stderr, "Error: opening outout file for writing: %d\n", errno);
	return -errno;
      }
      break;
      case 'w': w = parseInt(&argv[i][j+1]); break;*/
    case 'p':
      vectorWidth = parseDouble(&argv[i][j+1]);
      break;
    }
  }
  free(tempStr);
  /*if(rastarOut) {
    rastarBuffer = malloc(sizeof(float)*w*h);
    if(!rastarBuffer) {
      fprintf(stderr, "Error allocating memory for rastar buffer: %d\n", errno);
      return -errno;
    }
    for(i = 0;i < w * h;i++) rastarBuffer[i] = 0;
    }*/
  if(vectorOut)
    fprintf(vectorOut, "vn 0.0 0.0 -1\n");
  for(i = 0;i < 3;i++) initAxis(&axes[i]);
  read = getc(stdin);
  while(read != EOF) {
    if(read < 0 || read > 255) return -errno;
    if(read == '(') {
      while(read && read != ')') read = getc(stdin);//comment
    } else if(read <= 'Z' && read >= 'X') {
      if(axis == DEBUG_AXIS) printf("Pushing: %f (after char %c)\n", val, read);
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      axis = read - 'X';
      val = 0;
      mul = 1;
      commandType = CMD_IGNORE;
    } else if(read == 'P') {
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      val = 0;
      mul = 1;
      axis = 4;
      commandType = CMD_P;
    } else if(read == 'S') {
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      val = 0;
      mul = 1;
      axis = 4;
      commandType = CMD_S;
    } else if(read == 'G') {
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      val = 0;
      mul = 1;
      axis = 4;
      commandType = CMD_G_INCOMP;
    } else if(read == 'F') {
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      commandType = CMD_F;
      axis = 4;
      mul = 1;
      val = 0;
    } else if(read == 'M') {
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      commandType = CMD_M;
      axis = 4;
      mul = 1;
      val = 0;
    } else if(read >= '0' && read <= '9') {
      digit = mul * (read - '0');
      if(fabs(mul) > 0.5) val *= 10;
      else mul *= 0.1;
      val += digit;
    } else if(read == '.' && fabs(mul) > 0.5) {
      mul *= 0.1;
    } else if(read == '-' && axis != 4) {
      val = -0;
      mul = -1;
    } else if(read == '\n') {//delimiter character
      if(axis == DEBUG_AXIS) printf("Pushing: %f (after char %c)\n", val, read);
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      processPush();
      axis = 4;
      commandType = CMD_IGNORE;
    } else if(read == ' ' || read == 0x0d || read == '\t') {//ignore
    } else if(read == 'T') {
      if(!toolWarned)
	fprintf(stderr, "Warning: ignoring tool select commands (T)\n");
      toolWarned = 1;
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      processPush();
      axis = 4;
      commandType = CMD_IGNORE;
    } else if(read == ';') {//comment
      while(read >= 0 && read != '\n')
	read = getc(stdin);
      if(processVal(val, axis, commandType)) goto EndOfProgram;
      processPush();
      axis = 4;
      commandType = CMD_IGNORE;
    } else {
      fprintf(stderr, "Unknown character: '%c' (%d)\n", read, read);
    }
    if(axis == DEBUG_AXIS) printf("Ongoing parse: %f (after char %c)\n", val, read);
    read = getc(stdin);
  }
 EndOfProgram:
  processPush();//incase no newline at EOF
  for(i = 0;i < 3;i++) {
    fprintf(stdout, "%c:\n", 'X'+(int)i);
    for(j = 0;j < AXIS_EDGE_DEPTH;j++)
      fprintf(stdout, "\t%08.7f (%ld)", axes[i].min[j].val, axes[i].min[j].qty);
    fprintf(stdout, "\n");
    for(j = 0;j < AXIS_EDGE_DEPTH;j++)
      fprintf(stdout, "\t%08.7f (%ld)", axes[i].max[j].val, axes[i].max[j].qty);
    fprintf(stdout, "\n");
    fprintf(stdout, "\n");
  }
  fprintf(stdout, "Distance to travel %lf (%lf cutting)\nTime: %lf minutes (%lf cutting)\nAverage Speed: %lf\n",
	  travel, cutTravel, time, cutTime, travel/time);
  //free(rastarBuffer);
  if(vectorOut) fclose(vectorOut);
  //if(rastarOut) {//https://www.geeksforgeeks.org/c-program-to-write-an-image-in-pgm-format/
    //TODO paint
    //fclose(rastarOut);
  //}
}
