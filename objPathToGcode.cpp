#include <vector>
#include <stdio.h>
#include <stdlib.h>

struct Vert {
  char coord[64];
  long long a, b;//idx in verts
};

int main(int argc, char** argv) {
  std::vector<struct Vert> verts;
  char buffer[4096];
  char* line;
  size_t i, j, t;
  struct Vert temp;
  FILE* in = stdin;
  if(argc > 1) {
    in = fopen(argv[1], "rb");
    if(!in) {
      fprintf(stderr, "failed to open file %s\n", argv[1]);
      return 1;
    }
  }
  line = fgets(buffer, 4096, in);
  verts.emplace_back();//unused pad because obj is 1-based.
  while(line) {
    switch(line[0]) {
    case 'v':
      if(line[1] != ' ') continue;
      temp.a = temp.b = 0;
      i = 2;
      j = 0;
      temp.coord[j++] = 'x';
      while(line[i] != ' ') temp.coord[j++] = line[i++];
      temp.coord[j++] = ' ';
      temp.coord[j++] = 'y';
      i++;
      while(line[i] != ' ') temp.coord[j++] = line[i++];
      temp.coord[j++] = ' ';
      temp.coord[j++] = 'z';
      i++;
      while(line[i] != 0 && line[i] != '\n') temp.coord[j++] = line[i++];
      temp.coord[j++] = 0;
      verts.push_back(temp);
      break;
    case 'l':
      long long a = 0, b = 0;
      char* end;
      if(line[1] != ' ') continue;
      a = strtoll(&line[2], &end, 10);
      b = strtoll(end + 1, NULL, 10);
      if(a <= 0 || a >= verts.size() || b <= 0 || b >= verts.size()) {
	fprintf(stderr, "Error processing line: %s\n", line);
	return 1;
      }
      if(verts[a].a == 0) {
	verts[a].a = b;
      } else if(verts[a].b == 0) {
	verts[a].b = b;
      } else {
	fprintf(stderr, "Error processing line: %s; vert %lld (%s) has too many connections.\n", line, a, verts[a].coord);
      }
      if(verts[b].a == 0) {
	verts[b].a = a;
      } else if(verts[b].b == 0) {
	verts[b].b = a;
      } else {
	fprintf(stderr, "Error processing line: %s; vert %lld (%s) has too many connections.\n", line, b, verts[b].coord);
      }
      break;
    }
    line = fgets(buffer, 4096, in);
  }
  j = 0;
  for(i = 1;verts[i].b;i++);//find a vert with only one connection
  while(i) {
    printf("# %lu\ng1 %s\n", i, verts[i].coord);
    t = j == verts[i].a ? verts[i].b : verts[i].a;
    j = i;
    i = t;
  }
  fprintf(stderr, "warning: path only, all as g1 with no other config. Edit new file and adjust as needed, especially set feedrate (g1 f15) and turn on/off spindle (m3 10000 / m5)");
}
